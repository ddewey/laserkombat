; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CMainFrame
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "LevelEditor.h"
LastPage=0

ClassCount=5
Class1=CLevelEditorApp
Class2=CLevelEditorDoc
Class3=CLevelEditorView
Class4=CMainFrame

ResourceCount=2
Resource1=IDD_ABOUTBOX
Class5=CAboutDlg
Resource2=IDR_MAINFRAME

[CLS:CLevelEditorApp]
Type=0
HeaderFile=LevelEditor.h
ImplementationFile=LevelEditor.cpp
Filter=N

[CLS:CLevelEditorDoc]
Type=0
HeaderFile=LevelEditorDoc.h
ImplementationFile=LevelEditorDoc.cpp
Filter=N

[CLS:CLevelEditorView]
Type=0
HeaderFile=LevelEditorView.h
ImplementationFile=LevelEditorView.cpp
Filter=C
LastObject=CLevelEditorView
BaseClass=CView
VirtualFilter=VWC

[CLS:CMainFrame]
Type=0
HeaderFile=MainFrm.h
ImplementationFile=MainFrm.cpp
Filter=T
BaseClass=CFrameWnd
VirtualFilter=fWC
LastObject=ID_RUSTYWHITEBLOCK



[CLS:CAboutDlg]
Type=0
HeaderFile=LevelEditor.cpp
ImplementationFile=LevelEditor.cpp
Filter=D

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=5
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889
Control5=IDC_STATIC,static,1342308352

[MNU:IDR_MAINFRAME]
Type=1
Class=CMainFrame
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command4=ID_FILE_SAVE_AS
Command5=ID_FILE_MRU_FILE1
Command6=ID_APP_EXIT
Command7=ID_EDIT_UNDO
Command8=ID_EDIT_CUT
Command9=ID_EDIT_COPY
Command10=ID_EDIT_PASTE
Command11=ID_VIEW_TOOLBAR
Command12=ID_VIEW_STATUS_BAR
Command13=ID_APP_ABOUT
CommandCount=13

[ACL:IDR_MAINFRAME]
Type=1
Class=CMainFrame
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command4=ID_EDIT_UNDO
Command5=ID_EDIT_CUT
Command6=ID_EDIT_COPY
Command7=ID_EDIT_PASTE
Command8=ID_EDIT_UNDO
Command9=ID_EDIT_CUT
Command10=ID_EDIT_COPY
Command11=ID_EDIT_PASTE
Command12=ID_NEXT_PANE
Command13=ID_PREV_PANE
CommandCount=13

[TB:IDR_MAINFRAME]
Type=1
Class=CMainFrame
Command1=ID_GROUND
Command2=ID_WATER
Command3=ID_REDBLOCK
Command4=ID_RUSTYREDBLOCK
Command5=ID_TEE1
Command6=ID_TEE2
Command7=ID_TEE3
Command8=ID_TEE4
Command9=ID_TEE5
Command10=ID_TEE6
Command11=ID_TEE7
Command12=ID_MIRROR1
Command13=ID_MIRROR2
Command14=ID_MIRROR3
Command15=ID_MIRROR4
Command16=ID_NUKE
Command17=ID_ENEMEYNUKE
Command18=ID_STATIC
Command19=ID_RUSTY
Command20=ID_RAILVERT
Command21=ID_RAILHORIZ
Command22=ID_RAILCROSS
Command23=ID_TANK
Command24=ID_ENEMYTANK
Command25=ID_TANTANK
Command26=ID_WHITEBLOCK
Command27=ID_TRIANGLE1
Command28=ID_TRIANGLE2
Command29=ID_TRIANGLE3
Command30=ID_TRIANGLE4
Command31=ID_RUSTYTRIANGLE1
Command32=ID_RUSTYTRIANGLE2
Command33=ID_RUSTYTRIANGLE3
Command34=ID_RUSTYTRIANGLE4
Command35=ID_RUSTYBARSVERT
Command36=ID_RUSTYBARSHORIZ
Command37=ID_RUSTYWHITEBLOCK
CommandCount=37

