//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by LevelEditor.rc
//
#define IDD_ABOUTBOX                    100
#define IDR_MAINFRAME                   128
#define IDR_LEVELETYPE                  129
#define IDI_ICON1                       139
#define ID_GROUND                       32774
#define ID_WATER                        32775
#define ID_REDBLOCK                     32776
#define ID_RUSTYREDBLOCK                32777
#define ID_TEE1                         32778
#define ID_TEE2                         32779
#define ID_TEE3                         32780
#define ID_TEE4                         32781
#define ID_TEE5                         32782
#define ID_TEE6                         32783
#define ID_TEE7                         32784
#define ID_MIRROR1                      32785
#define ID_MIRROR2                      32786
#define ID_MIRROR3                      32787
#define ID_MIRROR4                      32788
#define ID_NUKE                         32790
#define ID_ENEMEYNUKE                   32791
#define ID_STATIC                       32792
#define ID_RUSTY                        32794
#define ID_RAILVERT                     32795
#define ID_RAILHORIZ                    32796
#define ID_RAILCROSS                    32797
#define ID_TANK                         32798
#define ID_ENEMYTANK                    32800
#define ID_TANTANK                      32801
#define ID_WHITEBLOCK                   32802
#define ID_TRIANGLE1                    32803
#define ID_TRIANGLE2                    32804
#define ID_TRIANGLE3                    32805
#define ID_TRIANGLE4                    32806
#define ID_RUSTYTRIANGLE1               32807
#define ID_RUSTYTRIANGLE3               32809
#define ID_RUSTYTRIANGLE4               32810
#define ID_RUSTYBARSVERT                32811
#define ID_RUSTYBARSHORIZ               32812
#define ID_RUSTYTRIANGLE2               32813
#define ID_RUSTYWHITEBLOCK              32814

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_3D_CONTROLS                     1
#define _APS_NEXT_RESOURCE_VALUE        140
#define _APS_NEXT_COMMAND_VALUE         32815
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
