// sound.cpp
#include <ddraw.h>
#include <time.h>
#include "pix.h"
#include "sound.h"

void Sound::Play()
{
	if (maxx==NOPLAY) return;
	PlaySound(NULL, NULL, NULL);
	if (sound) PlaySound(sound, NULL, SND_FILENAME|SND_ASYNC);
	maxx=NOPLAY;
	sound=NULL;

}
BOOL Sound::PlayASound(const char* name, SoundPriority priority)
{
	if (priority>maxx) {
		maxx=priority;
		sound=name;
		return TRUE;
	}
	return FALSE;
}

